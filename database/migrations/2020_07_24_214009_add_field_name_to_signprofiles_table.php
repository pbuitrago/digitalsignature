<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldNameToSignprofilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('signprofiles', function (Blueprint $table) {
            if (!Schema::hasColumn('signprofile', 'name')) {
                $table->string('name')->comment('nombre asigando al archivo');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('signprofiles', function (Blueprint $table) {
             if (Schema::hasColumn('signprofile', 'name')) {
                $table->dropColumn('name');
            };
        });
    }
}
