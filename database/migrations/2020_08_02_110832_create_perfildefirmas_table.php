<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfildefirmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfildefirmas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned()->nullable()
                      ->comment('Identificador del usuario');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->string('cert', 3000)->comment('Certificado firmante');
            $table->string('pkey',2000)->comment('Clave privada asociada al certificado del firmante');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfildefirmas');
    }
}
