<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/userprofile', function() {
	return view('userprofile');
})->name('userprofile');

Route::get('/readCertificate','SignProfileController@getCertificate')->name('readCertificate');
Route::get('/listcertificates', 'SignProfileController@listCertificate')->name('listcertificates');

Route::get('signprofile', 'SignProfileController@create')->name('signprofile');
Route::post('signprofilestore', 'SignProfileController@store')->name('signprofilestore');

Route::get('signFile', 'SignProfileController@signFile')->name('signFile');

Route::get('verifysign', 'SignProfileController@verifysign')->name('verifysign');
Route::get('verifysignd', 'SignProfileController@verifysign')->name('verifysign');