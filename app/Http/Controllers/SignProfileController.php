<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use App\Signprofile;
use App\Perfildefirma;
use App\User;


class SignProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('signprofile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $filename = Str::random(10) . '.p12';
        $path = $request->file('pkcs12')->storeAs('certificate', $filename);
        $url = 'storage/app/certificate/'.$filename;

        $certStore = file_get_contents('/home/pbuitrago/Cenditel/Proyecto-Murachi/digitalsignature/' . $url);
        $passphrase = 123456;

        if (!$certStore) {
            echo "Error: No se puede leer el fichero del certificado\n";
            exit;   
        }

        $pkcs12 = openssl_pkcs12_read($certStore, $certInfo, $passphrase );
        $cert = Crypt::encryptString($certInfo['cert']);
        $pkey = Crypt::encryptString($certInfo['pkey']);

       
        $profile = new Perfildefirma();
        $profile->cert = $cert; 
        $profile->pkey = $pkey;
        $profile->user_id = Auth::user()->id;
        $profile->save();
        Storage::disk('certificate')->delete($filename);
    
        return redirect()->route('userprofile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Storage::delete('file.jpg');
        //Storage::delete(['file.jpg', 'file2.jpg']);
        //Storage::disk('s3')->delete('folder_path/file_name.jpg'); especifica el disco donde se va eliminar
    }

    public function signFile() 
    {
        if(Auth::user()) {

            //Crear archivo pkcs#12
            $cert = Crypt::decryptString(Auth::user()->perfildefirma['cert']);
            $pkey = Crypt::decryptString(Auth::user()->perfildefirma['pkey']);
            $passphrase = 123456;
            $filename = Str::random(10) . '.p12';
            $storeCertificated = '/home/pbuitrago/Cenditel/Proyecto-Murachi/digitalsignature/storage/app/certificate/' . $filename;
            $createpkcs12 = openssl_pkcs12_export_to_file($cert,$storeCertificated,$pkey,$passphrase);
            
            //proceso de firma
            $pathPortableSigner = '/home/pbuitrago/Cenditel/Proyecto-Murachi/digitalsignature/PortableSigner/PortableSigner.jar';
            
            
            $namepdf = 'pruebaPDF.pdf';
            $namepdfsign = 'pruebaPDF-sign.pdf';
            $storePdfSign =  '/home/pbuitrago/Cenditel/Proyecto-Murachi/digitalsignature/storage/storepdfsign/' . $namepdfsign;
            $storePdf = '/home/pbuitrago/Cenditel/Proyecto-Murachi/digitalsignature/storage/storepdf/'. $namepdfsign;

            $comand = 'java -jar ' . $pathPortableSigner . ' -n -t ' . $storePdf . ' -o ' . $storePdfSign . ' -s ' . $storeCertificated . ' -p ' . $passphrase;
            
            $run = exec($comand);
            Storage::disk('certificate')->delete($filename);

        }

        else { return redirect()->route('login'); } 
    }


    public function getCertificate() {

        if(Auth::user()->perfildefirma) {

            $cert = Crypt::decryptString(Auth::user()->perfildefirma['cert']);
            $datos = openssl_x509_parse($cert);
        
            $certificateDetails = (object) [
                'subjCountry' => $datos['subject']['C'],
                'subjState' => $datos['subject']['ST'],
                'subjLocality' => $datos['subject']['L'],
                'subjOrganization' => $datos['subject']['O'],
                'subjUnitOrganization' => $datos['subject']['OU'],
                'subjName' => $datos['subject']['CN'],
                'subjMail' => $datos['subject']['emailAddress'],
                'issCountry' => $datos['issuer']['C'],
                'issState' => $datos['issuer']['ST'],
                'issLocality' => $datos['issuer']['L'],
                'issOrganization' => $datos['issuer']['O'],
                'issUnitOrganization' => $datos['issuer']['OU'],
                'issName' => $datos['issuer']['CN'],
                'issMail' => $datos['issuer']['emailAddress'],
                'version' => $datos['version'],
                'serialNumber' => $datos['serialNumber'],
                'validFrom' => $datos['validFrom'],
                'validTo' => $datos['validTo'],
                'signatureTypeSN' => $datos['signatureTypeSN'],
                'signatureTypeLN' => $datos['signatureTypeLN'],
                'signatureTypeNID' => $datos['signatureTypeNID'],
            ];

            print_r($certificateDetails); 

        } else { return redirect()->route('login'); }
    }

    public function listCertificate() {

        $users = User::all();
        $userlist = [];
        foreach ($users as $user) {

            print_r('############');
            print_r($user->name);
            print_r($user->email);

            if($user->signprofile) {
                print_r($user->signprofile['url']);
                print_r($user->signprofile['name']);
            }
            print_r('############');
         } 
    }

    public function verifysign() {

        $pdfUrl = '/home/pbuitrago/Cenditel/Proyecto-Murachi/digitalsignature/storage/storepdfsign/pruebaPDF-sign.pdf';
        $comand = 'pdfsig ' . $pdfUrl;
        $run = exec($comand, $output);
        $cont = 0;
        $size = count($output);
        foreach ($output as $data) {
            $array_data = array('data' => $output, );
            $json_test = json_encode($array_data);
            if(strpos($data,'Signature #')) {
                $cont ++;
            }
            print_r($data);
            print_r('......');
        }
        print_r($cont);
        print_r($size);
        $file = 'pdfsig.json';
        file_put_contents($file, $json_test);
        
    }
}
