<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfildefirma extends Model
{
     /**
     * Lista de atributos que pueden ser asignados masivamente
     * @var array $fillable
     */
    protected $fillable = ['user_id', 'cert', 'pkey'];

    /**
     * Busca el perfil de firma del
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
