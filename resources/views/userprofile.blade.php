@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Perfil del usuario') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @php
                        $user = Auth::user();
                    @endphp

                    <div>
                        <div class="row">
                            <div>
                                <h3 class="h4 border-bottom border-primary text-center pb-1">
                                    Datos personales
                                </h3>
                                <p>
                                    <span class="font-weight-bold"> Usuario: </span>
                                    <span class="text-gray"> {{ $user->name }} </span>
                                </p>
                                <p>
                                    <span class="font-weight-bold"> Correo: </span>
                                    <span class="text-gray"> {{ $user->email }} </span>
                                </p>
                                <p>
                                    <span class="font-weight-bold"> Certificado: </span>
                                    <a class="dropdown-item" href="{{ route('signprofile') }}">
                                        {{ __('Cargar certificado') }}
                                    </a>
                                    <span class="text-gray"> </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
